<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@include file = "admin.html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ship Details</title>
</head>
<body>
<html>
	<head>
		<title>Ship Details</title>
	</head>
	<body>
	<form action = "./ShipDetails">
	<fieldset>
	<legend><b>Ship Details</b></legend>
		<table border = "1" align = "center">
			<tr>
				<td align = "left">Ship ID</td>
				<td><input type = "text" name = shipid required></td>
			</tr>
			<tr>
				<td align = "left">Ship Name</td>
				<td><input type = "text" name = shipname required></td>
			</tr>
			<tr>
				<td align = "left">Company Name</td>
				<td><input type = "text" name = company_name required></td>
			</tr>
			<tr>
				<td align = "left">Model</td>
				<td><input type = "text" name = model required></td>
			</tr>
			<tr>
				<td align = "left">Length</td>
				<td><input type = "text" name = len required></td>
			</tr>
			<tr>
				<td align = "left">Width</td>
				<td><input type = "text" name = width required></td>
			</tr>
			<tr>
				<td align = "left">Weight</td>
				<td><input type = "text" name = weight required></td>
			</tr>
			<tr>
				<td align = "left">Draft</td>
				<td><input type = "text" name = draft required></td>
			</tr>
			<tr>
				<td align = "left">Flag</td>
				<td><input type = "text" name = flag required></td>
			</tr>
			<tr>
				<td align = "left">Made</td>
				<td><input type = "text" name = made required></td>
			</tr>
			<tr>
				<td align = "left">Number of Engines</td>
				<td><input type = "text" name = nofengs required></td>
			</tr>
			<tr>
				<td align = "left">Engine Type</td>
				<td><input type = "text" name = engtype required></td>
			</tr>
			<tr>
				<td align = "left">Engine Company</td>
				<td><input type = "text" name = engcompany required></td>
			</tr>
			<tr>
				<td align = "right"><input  type = submit  value = "save" /></td>
				<td><input  type = submit value = "clear" /></td>
			</tr>
		</table>
	</fieldset>
	</form>
	<a href="/ServShipDetails">List All</a>
</body>
</html>