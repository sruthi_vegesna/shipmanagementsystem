package com.serv;
import com.beans.*;
import com.dao.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServRegister
 */
@WebServlet("/ServRegister")
public class ServRegister extends HttpServlet {
    private static final long serialVersionUID = 1L;
      
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServRegister() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        System.out.println("<body bgcolor = '#c0c0c0'>");
        ArrayList<PojoRegister> el = Registerdao.getAll();
        //System.out.println("Begin");
        out.println("<table border = 1><tr><th>name</th><th>dateofbirth</th><th>designation</th><th>address</th><th>phnum</th><th>gender</th><th>emailid</th><th>password</th><th>securityq</th><th>securityans</th></tr>");
        for(PojoRegister u : el) {
            out.println("<tr><td>"+u.getName()+"</td><td>"+u.getDateofbirth()+"</td><td>"+u.getDesignation()+"</td><td>"+u.getAddress()+"</td><td>"+u.getPhnum()+"</td><td>"+u.getGender()+"</td><td>"+u.getEmailid()+"</td><td>"+u.getPassword()+"</td><td>"+u.getSecurityq()+"</td><td>"+u.getSecurityans()+"</td></tr>");
        }
        //out.println("</table>");
       
        }
        // TODO Auto-generated method stub
   

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request,response);
    }
}