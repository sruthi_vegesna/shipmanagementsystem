Problem Statement: 
	Developing Web Page on Ship Management System.
	
Abstract:
	1.It maintains Ship Details,Employee Details and the equipment details.

	2.It gives list of all employee work order details.

	3.It gives notifications on shipment status to the customer.
	
Modules:
	1.Administrator:
		This Administrator will maintain all the master information like Ships details,
		Employee Information, Employee work assignment details, Customer ship
		reservation details, Shipment details and Customer ship order details.
		
	2.Employee:
		Login process and he can check his work order schedule details and pay
		bill details.
		
	3.Stores Department:
		In this module the Store Department will maintain the ship equipment 
		details, item details and purchase order details for item stock.
		
	4.Customer:
		In this module Customer can register in the application. He can book
		the cargo details, he can reserve the ship for traveling.
		
Roles:
	Group-19
	V.V.L.Sruthi - Administrator ,Customer Modules.
	S.Deepa - Employee Module.
	S.Manasa-Stores Department.
	
Technologies Used:
	HTML,CSS,JavaScript,Java,Servlets,Database.