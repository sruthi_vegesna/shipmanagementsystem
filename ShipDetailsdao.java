package com.dao;
import java.sql.*;
import java.util.ArrayList;
import java.io.*;

import com.beans.*;
import com.factory.MyConnectionLogin;
import com.serv.ShipDetails;
public class ShipDetailsdao {
    public static boolean insertShipDetails(PojoShipDetails q) {
        try
        {
            Connection con = MyConnectionLogin.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into ship_details values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setInt(1,q.getShipid() );
            ps.setString(2, q.getShipname());
            ps.setString(3, q.getCompany_name());
            ps.setString(4, q.getModel());
            ps.setString(5, q.getLen());
            ps.setString(6, q.getWidth());
            ps.setString(7, q.getWeight());
            ps.setString(8, q.getDraft());
            ps.setString(9, q.getFlag());
            ps.setString(10, q.getMade());
            ps.setString(11, q.getNofengs());
            ps.setString(12,q.getEngtype());
            ps.setString(13,q.getEngcompany());
            ps.executeUpdate();
            return true;
        }
        catch(Exception ls){
            System.out.println(ls);
        }
        return false;
       
                   
        }
    public static ArrayList<PojoShipDetails> getAll() {
        ArrayList<PojoShipDetails> al= new ArrayList<PojoShipDetails>();
        try
        {
            Connection con =MyConnectionLogin.getConnection();
    Statement st =con.createStatement();
    ResultSet rs= st.executeQuery("select * from ship_details");
    while(rs.next())
    {
    	PojoShipDetails u=new PojoShipDetails();
   
    u.setShipid(rs.getInt("shipid"));
    u.setShipname(rs.getString("shipname"));
    u.setCompany_name(rs.getString("company_name"));
    u.setModel(rs.getString("model"));
    u.setLen(rs.getString("len"));
    u.setWidth(rs.getString("width"));
    u.setWeight(rs.getString("weight"));
    u.setDraft(rs.getString("draft"));
    u.setFlag(rs.getString("flag"));
    u.setMade(rs.getString("made"));
    u.setNofengs(rs.getString("nofengs"));
    u.setEngtype(rs.getString("engtype"));
    u.setEngcompany(rs.getString("engcompany"));
    al.add(u);
        }
        return al;
    }
catch(Exception es){ System.out.println(es);}
return null;
}
    public static ArrayList<PojoShipDetails> getOne(PojoShipDetails u) {
        System.out.println("Checked");
        ArrayList<PojoShipDetails> al= new ArrayList<PojoShipDetails>();
        try
        {
            Connection con =MyConnectionLogin.getConnection();
    PreparedStatement st =con.prepareStatement("select * from ship_details where shipid=? and shipname=? and company_name=? and model = ? and len = ? and width = ? and weight = ? and draft = ? and flag = ? and made = ? and nofengs = ? and engtype = ? and engcompany = ? ");
    st.setInt(1, u.getShipid());
    st.setString(2, u.getShipname());
    st.setString(3,u.getCompany_name());
    st.setString(4,u.getModel());
    st.setString(5,u.getLen());
    st.setString(6,u.getWidth());
    st.setString(7,u.getWeight());
    st.setString(8,u.getDraft());
    st.setString(9,u.getFlag());
    st.setString(10,u.getMade());
    st.setString(11,u.getNofengs());
    st.setString(12,u.getEngtype());
    st.setString(13,u.getEngcompany());
    
    
    ResultSet rs= st.executeQuery();
   
    System.out.println("Checked");
    while(rs.next())
    {
        PojoShipDetails uu=new PojoShipDetails();
   
    
    
    uu.setShipid(rs.getInt("shipid"));
    uu.setShipname(rs.getString("shipname"));
    uu.setCompany_name(rs.getString("company_name"));
    uu.setModel(rs.getString("model"));
    uu.setLen(rs.getString("len"));
    uu.setWidth(rs.getString("width"));
    uu.setWeight(rs.getString("weight"));
    uu.setDraft(rs.getString("draft"));
    uu.setFlag(rs.getString("flag"));
    uu.setMade(rs.getString("made"));
    uu.setNofengs(rs.getString("nofengs"));
    uu.setEngtype(rs.getString("engtype"));
    uu.setEngcompany(rs.getString("engcompany"));
   
    al.add(uu);
        }
        return al;
    }
catch(Exception es){ }
return null;
}
}