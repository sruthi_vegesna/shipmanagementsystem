package com.dao;
import java.sql.*;
import java.util.ArrayList;
import java.io.*;

import com.beans.*;
import com.factory.MyConnectionLogin;

public class LoginDao {
	public static boolean insertLoginpojo(Loginpojo e) {
		try {
			Connection con = MyConnectionLogin.getConnection();
			PreparedStatement ps = con.prepareStatement("insert into adminlogin values(?,?,?)");
			ps.setString(1, e.getUsername());
			ps.setString(2, e.getPassword());
			ps.setString(3, e.getRole());
			ps.executeUpdate();
			return true;
		}
		catch(Exception es) {
		System.out.println(es);
		}
		return false;
	}

	public static ArrayList<Loginpojo> getOne(Loginpojo e) {
		// TODO Auto-generated method stub
		System.out.println(e.getUsername()+" "+e.getPassword()+" "+e.getRole());
	ArrayList<Loginpojo> alp=new ArrayList<Loginpojo>();
	try
	{
		Connection con=MyConnectionLogin.getConnection();
		System.out.println("Conn");
		PreparedStatement ps=con.prepareStatement("select * from adminlogin where username = ? and pwd = ? and Role = ?");
		System.out.println("see");
		ps.setString(1, e.getUsername());
		ps.setString(2, e.getPassword());
		ps.setString(3,e.getRole());
		ResultSet rs=ps.executeQuery();
		System.out.println("execu");
		while(rs.next())
		{
			Loginpojo lp=new Loginpojo();
			lp.setUsername(rs.getString("username"));
			lp.setPassword(rs.getString("pwd"));
			lp.setRole(rs.getString("Role"));
			alp.add(lp);
		}
		return alp;
	}
	catch(Exception eq){System.out.println(eq);}
	return null;
} 

}