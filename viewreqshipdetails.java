package com.serv;
import com.beans.*;
import com.dao.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ViewReqShipDetails
 */
@WebServlet("/ViewReqShipDetails")
public class ViewReqShipDetails extends HttpServlet {
    private static final long serialVersionUID = 1L;
      
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewReqShipDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        //System.out.println("Begin");
        ArrayList<ViewPojoShipDetails> el = ViewShipDetailsdao.getAll();
        //System.out.println("Begin");
        out.println("<table border = 1><tr><th>shipid</th><th>shipname</th><th>company_name</th><th>model</th><th>len</th><th>width</th><th>weight</th><th>draft</th><th>flag</th><th>made</th><th>nofengs</th>engtype</th><th>engcompany</th></tr>");
        for(ViewPojoShipDetails u : el) {
            out.println("<tr><td>"+u.getShipid()+"</td><td>"+u.getShipname()+"</td><td>"+u.getCompany_name()+"</td><td>"+u.getModel()+"</td><td>"+u.getLen()+"</td><td>"+u.getWidth()+"</td><td>"+u.getWeight()+"</td><td>"+u.getDraft()+"</td><td>"+u.getFlag()+"</td><td>"+u.getMade()+"</td><td>"+u.getNofengs()+"</td><td>"+u.getEngtype()+"</td><td>"+u.getEngcompany()+"</td></tr>");
        }
        //out.println("</table>");
       
        }
        // TODO Auto-generated method stub
   

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request,response);
    }

}