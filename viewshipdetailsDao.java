package com.dao;
import java.sql.*;
import java.util.ArrayList;
import java.io.*;

import com.beans.*;
import com.factory.MyConnectionLogin;
import com.serv.ShipDetails;
public class ViewShipDetailsdao {
    public static boolean insertViewShipDetails(ViewPojoShipDetails q) {
       
       
        return false;
       
                   
        }
    public static ArrayList<ViewPojoShipDetails> getAll() {
        ArrayList<ViewPojoShipDetails> al= new ArrayList<ViewPojoShipDetails>();
        try
        {
            Connection con =MyConnectionLogin.getConnection();
    Statement st =con.createStatement();
    ResultSet rs= st.executeQuery("select * from ship_details");
    while(rs.next())
    {
        ViewPojoShipDetails u=new ViewPojoShipDetails();
  
    u.setShipid(rs.getInt("shipid"));
    u.setShipname(rs.getString("shipname"));
    u.setCompany_name(rs.getString("company_name"));
    u.setModel(rs.getString("model"));
    u.setLen(rs.getString("len"));
    u.setWidth(rs.getString("width"));
    u.setWeight(rs.getString("weight"));
    u.setDraft(rs.getString("draft"));
    u.setFlag(rs.getString("flag"));
    u.setMade(rs.getString("made"));
    u.setNofengs(rs.getString("nofengs"));
    u.setEngtype(rs.getString("engtype"));
    u.setEngcompany(rs.getString("engcompany"));
    al.add(u);
        }
        return al;
    }
catch(Exception es){ System.out.println(es);}
return null;
}
    public static ArrayList<ViewPojoShipDetails> getOne(ViewPojoShipDetails u) {
        System.out.println("Checked");
        ArrayList<ViewPojoShipDetails> al= new ArrayList<ViewPojoShipDetails>();
        try
        {
            Connection con =MyConnectionLogin.getConnection();
    PreparedStatement st =con.prepareStatement("select * from ship_details where shipid=? and shipname=? and company_name=? and model = ? and len = ? and width = ? and weight = ? and draft = ? and flag = ? and made = ? and nofengs = ? and engtype = ? and engcompany = ?");
    st.setInt(1, u.getShipid());
    st.setString(2, u.getShipname());
    st.setString(3,u.getCompany_name());
    st.setString(4,u.getModel());
    st.setString(5,u.getLen());
    st.setString(6,u.getWidth());
    st.setString(7,u.getWeight());
    st.setString(8,u.getDraft());
    st.setString(9,u.getFlag());
    st.setString(10,u.getMade());
    st.setString(11,u.getNofengs());
    st.setString(12,u.getEngtype());
    st.setString(13,u.getEngcompany());
    ResultSet rs= st.executeQuery();
   
    System.out.println("Checked");
    while(rs.next())
    {
        ViewPojoShipDetails uu=new ViewPojoShipDetails();
   
        uu.setShipid(rs.getInt("shipid"));
        uu.setShipname(rs.getString("shipname"));
        uu.setCompany_name(rs.getString("company_name"));
        uu.setModel(rs.getString("model"));
        uu.setLen(rs.getString("len"));
        uu.setWidth(rs.getString("width"));
        uu.setWeight(rs.getString("weight"));
        uu.setDraft(rs.getString("draft"));
        uu.setFlag(rs.getString("flag"));
        uu.setMade(rs.getString("made"));
        uu.setNofengs(rs.getString("nofengs"));
        uu.setEngtype(rs.getString("engtype"));
        uu.setEngcompany(rs.getString("engcompany"));
        al.add(uu);
        }
        return al;
    }
catch(Exception es){ }
return null;
}
}