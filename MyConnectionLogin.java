package com.factory;
import java.sql.*;

public class MyConnectionLogin {
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://local host:3306/ship","root","root");
			return con;
		}
		catch(Exception e) {
			return null;
		}
	}
}