package com.serv;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beans.PojoRegister;
import com.dao.Registerdao;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	            //PrintWriter out = response.getWriter();
	            //out.println("welcome");
	            PrintWriter out=response.getWriter();
	            response.setContentType("text/html");
	            String name = request.getParameter("name");
	            String dateofbirth = request.getParameter("dateofbirth");
	            String designation=request.getParameter("designation");
	            String address =request.getParameter("address");
	            int phnum =Integer.parseInt(request.getParameter("phnum"));
	            String gender=request.getParameter("gender");
	            String emailid=request.getParameter("emailid");
	            String password=request.getParameter("password");
	            String Securityq =request.getParameter("securityq");
	            String Securityans=request.getParameter("securityans");
	            PojoRegister q= new PojoRegister();
	            
	            
	            q.setName(name);
	            q.setDateofbirth(java.sql.Date.valueOf(dateofbirth));
	            q.setDesignation(designation);
	            q.setAddress(address);
	            q.setPhnum(phnum);
	            q.setGender(gender);
	            q.setEmailid(emailid);
	            q.setPassword(password);
	            q.setSecurityq(Securityq);
	            q.setSecurityans(Securityans);
	            boolean b = Registerdao.insertRegister(q);
	            if (b == true) {
	                RequestDispatcher rd = request.getRequestDispatcher("CustomerHome.jsp");
	                rd.forward(request, response);
	        }
	            else {
	                out.println("record not found");
	               
	            }
	         
	}

}
