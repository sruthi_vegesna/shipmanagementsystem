package com.dao;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;

import java.io.*;

import com.beans.*;
import com.factory.MyConnectionLogin;
import com.serv.RegisterServlet;
public class Registerdao {
    public static boolean insertRegister(PojoRegister q) {
        try
        {
            Connection con = MyConnectionLogin.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into registration values(?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1,q.getName());
            ps.setDate(2, q.getDateofbirth());
            ps.setString(3, q.getDesignation());
            ps.setString(4, q.getAddress());
            ps.setInt(5, q.getPhnum());
            ps.setString(6, q.getGender());
            ps.setString(7, q.getEmailid());
            ps.setString(8, q.getPassword());
            ps.setString(9, q.getSecurityq());
            ps.setString(10,q.getSecurityans());
            ps.executeUpdate();
            return true;
        }
        catch(Exception ls){
            System.out.println(ls);
        }
        return false;
       
                   
        }
    public static ArrayList<PojoRegister> getAll() {
        ArrayList<PojoRegister> al= new ArrayList<PojoRegister>();
        try
        {
            Connection con =MyConnectionLogin.getConnection();
    Statement st =con.createStatement();
    ResultSet rs= st.executeQuery("select * from registration");
    while(rs.next())
    {
    	PojoRegister u=new PojoRegister();
   
    u.setName(rs.getString("name"));
    u.setDateofbirth(rs.getDate("dateofbirth"));
    u.setDesignation(rs.getString("designation"));
    u.setAddress(rs.getString("address"));
    u.setPhnum(rs.getInt("phnum"));
    u.setGender(rs.getString("gender"));
    u.setEmailid(rs.getString("emailid"));
    u.setPassword(rs.getString("password"));
    u.setSecurityq(rs.getString("securityq"));
    u.setSecurityans(rs.getString("securityans"));
    al.add(u);
        }
        return al;
    }
catch(Exception es){ System.out.println(es);}
return null;
}
    public static ArrayList<PojoRegister> getOne(PojoRegister u) {
        System.out.println("Checked");
        ArrayList<PojoRegister> al= new ArrayList<PojoRegister>();
        try
        {
            Connection con =MyConnectionLogin.getConnection();
    PreparedStatement st =con.prepareStatement("select * from registration where name=? and dateofbirth=? and designation=? and address = ? and phnum = ? and gender = ? and emailid = ? and password = ? and securityq = ? and sequrityans = ? ");
    st.setString(1, u.getName());
    st.setDate(2, u.getDateofbirth());
    st.setString(3,u.getDesignation());
    st.setString(4,u.getAddress());
    st.setInt(5,u.getPhnum());
    st.setString(6,u.getGender());
    st.setString(7,u.getEmailid());
    st.setString(8,u.getPassword());
    st.setString(9,u.getSecurityq());
    st.setString(10,u.getSecurityans());
    ResultSet rs= st.executeQuery();
   
    System.out.println("Checked");
    while(rs.next())
    {
        PojoRegister uu=new PojoRegister();
   
    
    
    uu.setName(rs.getString("name"));
    uu.setDateofbirth(rs.getDate("dateofbirth"));
    uu.setDesignation(rs.getString("designation"));
    uu.setAddress(rs.getString("address"));
    uu.setPhnum(rs.getInt("phnum"));
    uu.setGender(rs.getString("gender"));
    uu.setEmailid(rs.getString("emailid"));
    uu.setPassword(rs.getString("password"));
    uu.setSecurityq(rs.getString("securityq"));
    uu.setSecurityans(rs.getString("securityans"));
    al.add(uu);
        }
        return al;
    }
catch(Exception es){ }
return null;
}
}
