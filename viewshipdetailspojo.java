package com.beans;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

public class ViewPojoShipDetails {
    private int shipid;
    private String shipname;
    private String company_name;
    private String model;
    private String len;
    private String width;
    private String weight;
    private String draft;
    private String flag;
    private String made;
    private String nofengs;
    private String engtype;
    private String engcompany;
    
    public ViewPojoShipDetails(int shipid, String shipname,String company_name,String model,String len,String width,String weight,String draft,String flag,String made,String nofengs,String engtype,String engcompany) {
        this.shipid = shipid;
        this.shipname = shipname;
        this.company_name = company_name;
        this.model = model;
        this.len = len;
        this.width = width;
        this.weight = weight;
        this.draft = draft;
        this.flag = flag;
        this.made = made;
        this.nofengs = nofengs;
        this.engtype = engtype;
        this.engcompany = engcompany;
    }
    public ViewPojoShipDetails() {
        // TODO Auto-generated constructor stub
    }
    public int getShipid() {
        return shipid;
    }
    public void setShipid(int shipid) {
        this.shipid = shipid;
    }
    public String getShipname() {
        return shipname;
    }
    public void setShipname(String shipname) {
        this.shipname = shipname;
    }
    public String getCompany_name() {
        return company_name;
    }
    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getLen() {
        return len;
    }
    public void setLen(String len) {
        this.len = len;
    }
    public String getWidth() {
        return width;
    }
    public void setWidth(String width) {
        this.width = width;
    }
    public String getWeight() {
        return weight;
    }
    public void setWeight(String weight) {
        this.weight = weight;
    }
    public String getDraft() {
        return draft;
    }
    public void setDraft(String draft) {
        this.draft = draft;
    }
    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }
    public String getMade() {
        return made;
    }
    public void setMade(String made) {
        this.made = made;
    }
    public String getNofengs() {
        return nofengs;
    }
    public void setNofengs(String nofengs) {
        this.nofengs = nofengs;
    }
    public String getEngtype() {
        return engtype;
    }
    public void setEngtype(String engtype) {
        this.engtype = engtype;
    }
    public String getEngcompany() {
        return engcompany;
    }
    public void setEngcompany(String engcompany) {
        this.engcompany = engcompany;
    }
}