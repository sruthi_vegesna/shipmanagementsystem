package com.serv;
import com.dao.*;
import com.beans.*;

import java.io.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
/**
 * Servlet implementation class ShipDetails
 */
@WebServlet("/ShipDetails")
public class ShipDetails extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String ShipDetails = null;

    /**
     * Default constructor.
     */
    public ShipDetails() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param <HttpServletRequest>
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected  void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out=response.getWriter();
        response.setContentType("text/html");
        int shipid = Integer.parseInt(request.getParameter("shipid"));
        String shipname = request.getParameter("shipname");
        String company_name=request.getParameter("company_name");
        String model =request.getParameter("model");
        String len =request.getParameter("len");
        String width =request.getParameter("width");
        String weight=request.getParameter("weight");
        String draft=request.getParameter("draft");
        String flag=request.getParameter("flag");
        String made=request.getParameter("made");
        String nofengs=request.getParameter("nofengs");
        String engtype=request.getParameter("engtype");
        String engcompany=request.getParameter("engcompany");
        PojoShipDetails q= new PojoShipDetails();
        q.setShipid(shipid);
        q.setShipname(shipname);
        q.setCompany_name(company_name);
        q.setModel(model);
        q.setLen(len);
        q.setWidth(width);
        q.setWeight(weight);
        q.setDraft(draft);
        q.setFlag(flag);
        q.setMade(made);
        q.setNofengs(nofengs);
        q.setEngtype(engtype);
        q.setEngcompany(engcompany);
        boolean b = ShipDetailsdao.insertShipDetails(q);
        if (b == true) {
            RequestDispatcher rd = request.getRequestDispatcher("/admin.jsp");
            rd.forward(request, response);
    }
        else {
            out.println("record not found");
           
        }
       
       
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

}
